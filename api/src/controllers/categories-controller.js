import express from 'express';
import multer from 'multer';
import storage from '../storage.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import { validatorMiddleware, addCategorySchema } from '../validators/index.js';
import { roles } from '../common/users.js';

const createCategoriesController = (categoriesService, categoriesData) => {

  const categoriesController = express.Router();
  categoriesController.use(authMiddleware);

  categoriesController
    .get('/', async (req, res, next) => {
      const { categories, error } = await categoriesService.getCategories(categoriesData)();

      return error ? next(error) : res.status(200).send(categories);
    })
    .get('/:id', async (req, res, next) => {
      const { id } = req.params;
      const { category, error } = await categoriesService.getCategory(categoriesData)(+id);
      return error ? next(error) : res.status(200).send(category);
    })
    .post('/', roleMiddleware(roles.ADVANCED_USER_ROLE), multer({ storage: storage }).single('cover'), validatorMiddleware(addCategorySchema), async (req, res, next) => {
      const { name, description } = req.body;
      const { filename } = req.file;
      const { id } = req.user;
      const { error, category } = await categoriesService.addCategory(categoriesData)(name, description, filename, +id);

      return error ? next(error) : res.status(201).send(category);
    });
  return categoriesController;
};
export default createCategoriesController;
