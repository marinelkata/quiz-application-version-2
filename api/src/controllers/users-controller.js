import express from 'express';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import { roles } from '../common/users.js';

const createUsersController = (usersService, usersData) => {

  const usersController = express.Router();
  usersController.use(authMiddleware);

  usersController
  .get('/leaderboard',roleMiddleware(roles.DEFAULT_USER_ROLE), async (req, res, next) => {
    const query = req.query;

    const { error, users } = await usersService.getLeaderboard(usersData)(query);
    return error ? next(error) : res.status(200).send(users);
  })
    .get('/:id', authMiddleware, async (req, res, next) => {
      const { id } = req.params;

      const { error, user } = await usersService.getUserById(usersData)(+id, req.user);

      return error ? next(error) : res.status(200).send(user);
    })
    .get('/:id/history', async (req, res, next) => {
      const { id } = req.params;
      const user = req.user;
      const query = req.query;
      const { error, history } = await usersService.getHistory(usersData)(+id, user, query);

      return error ? next(error) : res.status(200).send(history);
    });
  return usersController;
};
export default createUsersController;

