/* eslint-disable no-unused-expressions */
/* eslint-disable complexity */
const addQuizSchema = {
  name: value => {
    if (!value) {
      return 'Name is required!';
    }

    if (typeof value !== 'string' || value.trim().length < 5 || value.trim().length > 200) {
      return 'Name should be a string in range [5..200]';
    }

    return null;
  },
  duration: value => {
    if (!value) {
      return 'Duration is required!';
    }

    if (isNaN(value) || value < 1 || value > 60 * 60) {
      return 'Duration should be in range [1..60] min';
    }

    return null;
  },
  questions: value => {
    if (!value) {
      return 'Questions is required!';
    }

    if (!Array.isArray(value) || value.length < 2) {
      return 'Questions must be a colletion of at least 2';
    }

    const messages = value.reduce((acc, question) => {
      if (!question.text || typeof question.text !== 'string' || question.text.trim().length < 1 || question.text.trim().length > 500) {
        acc.push('Every question must have text that should be a string in range [1..300]');
      }
      if (!question.points || isNaN(question.points) || question.points < 1 || question.points > 6) {
        acc.push('Every question must have points that should be a number in range [1..6]');
      }
      if (!question.type || typeof question.type !== 'string' || (question.type !== 'single' && question.type !== 'multiple')) {
        acc.push('Every question must have type that should be either single or multiple');
      }
      if (!question.answers || !Array.isArray(question.answers) || question.answers.length < 2) {
        acc.push('Every question must have at least two possible answers');
      }
      if (question.answers.some(answer => !answer.text || typeof answer.text !== 'string'
        || answer.text.trim().length < 1 || question.text.trim().length > 300 || answer.value === null || typeof answer.value !== 'boolean')) {
        acc.push('Every answer must have text that should be a string in range [1..300] and value type of boolean');
      }
      const corrrectAnswers = question.answers.reduce((acc, answer) => {
        answer.value ? acc++ : '';
        return acc;
      }, 0);

      if ((question.type === 'single' && corrrectAnswers !== 1) || (question.type === 'multiple' && corrrectAnswers < 1)) {
        acc.push('Every single-choice question must have only one correct answer and every multiple-choice - at least 1');
      }

      return acc;
    }, []);
    const result = messages.filter((text, index, array) => array.indexOf(text) === index);

    if (result.length) {
      return result.join('. ');
    }


    return null;

  },
};

export { addQuizSchema };