import pool from './pool.js';

const getAll = async () => {

    const sql1 = `
    SELECT c.id, c.name, c.description, c.cover_path,
    COUNT(q.id) AS quizzes FROM categories AS c
    LEFT JOIN quizzes as q ON c.id = q.categories_id
    GROUP BY c.id;
    `;

    const sql2 = `
    SELECT COUNT(*) as count FROM categories;
    `;

    const categories = await pool.query(sql1);
    const count = await pool.query(sql2);

    return { categories, count: count[0].count };

};

const getBy = async (column, value) => {

    const sql = `
    SELECT * FROM categories
    WHERE ${column} = ?;
    `;

    const category = await pool.query(sql, [value]);

    return category[0];
};

const add = async (name, description, path, userId) => {
    const sql = `
        INSERT INTO categories(name, description, users_id, cover_path)
        VALUES (?,?,?,?);
    `;

    const result = await pool.query(sql, [name, description, userId, path]);

    return {
        id: result.insertId,
        name,
        description,
        cover_path: path,
        quizzes: 0,
    };
};


export default {
    getAll,
    getBy,
    add,
};