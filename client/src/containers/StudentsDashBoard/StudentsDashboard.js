import React, { useState, useEffect, useContext } from 'react';
import Categories from '../../components/Categories/Categories';
import Loading from '../../components/Loading/Loading';
import httpProvider from '../../providers/httpProvider';
import { Grid, Box, Paper, Link,} from '@material-ui/core';
import {BASE_URL} from '../../common/constants';
import UserHistory from '../../components/UserHistory/UserHistory';
import LeaderboardBox from '../../components/Leaderboard/LeaderboardBox';
import AuthContext from '../../context/AuthContext';
import Leaderboard from '../../components/Leaderboard/Leaderboard';
import StudentsHistory from '../../components/StudentsHistory/StudentsHistory';
import AlertDialog from '../../components/AlertDialog/AlertDialog';
import { makeStyles } from '@material-ui/core/styles';
import AppError from '../../components/AppError/AppError';
import useCustomQueryParams from '../../custom-hooks/useCustomQueryParams';
import { useHistory } from "react-router-dom";
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    marginLeft: 50
  },
  container: {
    paddingRight: 100,
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'row',
  },
  fixedHeight: {
    height: 100,
  },
  categoriesContainer: {
    display: 'flex',
  },
  category: {
    flexDirection: 'row',
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6),
  },
  history: {
    height: 410,
    marginRight: 80,
  },
  historyPage: {
    marginLeft: 350,
    marginTop: 30,
    width: 600
  },
  grid: {
    marginLeft: 110,
  },
  gridItem: {
    padding: "0 15px !important"
  },
}));

const StudentsDashboard = (props) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [dashboardData, setDashboardData] = useState(null);
  const { user } = useContext(AuthContext);
  const {page} = useCustomQueryParams();
  const [alert, setAlert] = useState({open:false, msg: null});
  const history = useHistory();
  const searchParams = history.location.search.slice(8);

  const setQuerryParams = (parameter, value) => {
    let currentUrlParams = new URLSearchParams(props.location.search);
    currentUrlParams.set(parameter, value);
    history.push({ search: '?' + currentUrlParams.toString() });
  }

  useEffect(() => {
    setLoading(true);
    setDashboardData(null);

    httpProvider.getAll(`${BASE_URL}/categories`, `${BASE_URL}/users/leaderboard?page=${page}&${searchParams}`, `${BASE_URL}/users/${user.sub}/history?page=${page}&${searchParams}`)
    .then(res => {
      setDashboardData(res);
    })
    .catch(error => setError(error.message))
    .finally(() => setLoading(false))
  }, [page, searchParams]);

  if (loading) {
    return <Loading />
  }

  if(error) {
    return <AppError message={error} />;
  }

  if(dashboardData && location.pathname === '/quizzes/history') {
    return (
      <div className={classes.historyPage} container>
        <StudentsHistory data={dashboardData[2]} setQuerryParams={setQuerryParams} />
      </div>
    )
  }

  if(dashboardData && location.pathname === '/users/leaderboard') {
    if(dashboardData[1]) {
      return <Leaderboard data={dashboardData[1]} pages={{page}} setQuerryParams={setQuerryParams} />
    }
  }

  return (
    <div maxWidth="md" className={classes.root}>
      <AlertDialog alert={alert} close={() => setAlert({open:false, msg: null})} />
      <Grid container className={classes.grid}>
        {dashboardData ? <Categories xs={8} md={1} className={classes.gridItem} categories={dashboardData[0].categories} /> : null}
      </Grid>
      <Box className={classes.container} spacing={1}>
        <Grid>
          <Paper style={{width: 400}}>
            {dashboardData ? <LeaderboardBox data={dashboardData[1]} /> : null}
          </Paper>
        </Grid>
        <Grid item xs={6} sm={6} className={classes.history}>
          <Paper style={{width: 400, marginTop: 30}}>
            {dashboardData ? <UserHistory data={dashboardData[2].history} /> : null}
            <Link href='/quizzes/history?page=1'>See history</Link>
          </Paper>
        </Grid>
      </Box>
    </div>
  )
};

StudentsDashboard.propTypes = {
  location: PropTypes.object.isRequired,
};

export default StudentsDashboard;
