import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import RemoveIcon from '@material-ui/icons/Remove';
import PropTypes from 'prop-types';
import { FormControl, FormHelperText } from '@material-ui/core';


const Answer = ({answer, deleteDisabled, questionId, handleChangeInput, handleRemoveFields}) => {
  
    return (
      <FormControl error={!answer.text.valid && answer.text.touched}>
        <FormHelperText>{answer.text.errorMsg}</FormHelperText>
        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <div className="input-group-text">
              <input type="checkbox" disabled={!answer.text.value} checked={answer.value.value} aria-label="Checkbox for following text input" onChange={event => handleChangeInput(event, 'answer',questionId, answer.id.value)} />
            </div>
          </div>
          <input
            style={!(answer.text.valid) && answer.text.touched ? { border: '1px solid red'} : { font: 'italic'}}
            type="text"
            className="form-control" 
            name='text' 
            aria-label="Text input with checkbox" 
            placeholder="Write possible answer here..."
            onClick={event => handleChangeInput({target: {name: 'text', value: event.target.value || ''}}, 'answer',questionId, answer.id.value)}
            onChange={event => handleChangeInput(event, 'answer',questionId, answer.id.value)}
          />
          <div className="input-group-append">
            <IconButton class="btn btn-outline-secondary" disabled={deleteDisabled} onClick={() => handleRemoveFields('answer',questionId, answer.id.value)}>
              <RemoveIcon />
            </IconButton>
          </div> 
        </div>
      </FormControl>
    )
}
export default Answer;

Answer.propTypes = {
    handleChangeInput: PropTypes.func.isRequired,
    handleRemoveFields: PropTypes.func.isRequired,
    questionId : PropTypes.number.isRequired,
    deleteDisabled:PropTypes.bool.isRequired,
    answer : PropTypes.object.isRequired,
  }
