import React from 'react';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import { Grid, FormControl, Select, InputLabel, MenuItem, CardContent, Card, FormGroup, FormHelperText } from '@material-ui/core';
import DeleteIcon from "@material-ui/icons/Delete";
import PropTypes from 'prop-types';
import Answer from '../Answer/Answer';

const Question = ({ classes, number, question, handleChangeInput, handleRemoveFields, handleAddFields }) => {

  const points = [
    { name: 'One', value: 1 },
    { name: 'Two', value: 2 },
    { name: 'Three', value: 3 },
    { name: 'Four', value: 4 },
    { name: 'Five', value: 5 },
    { name: 'Six', value: 6 }
  ];

  const types = [
    { name: 'Single-choice', value: 'single' },
    { name: 'Multiple-choice', value: 'multiple' },
  ]

  return (
    <FormControl error className={classes.myCard}>
      <Card className={classes.myCard}>
        <IconButton onClick={() => handleRemoveFields('question', question.id.value)}>
          <DeleteIcon />
        </IconButton>
        <span style={{ 'font-size': '20px' }}>{`Question ${number}`}</span>
        <CardContent>
          <Grid className={classes.formControl}>
            <TextField
              error={!question.text.valid && question.text.touched}
              helperText={question.text.errorMsg ? question.text.errorMsg : null}
              name="text"
              label="Question"
              value={question.text.value}
              onClick={(event) => handleChangeInput({target: {name: 'text', value: event.target.value || ''}},'question', question.id.value)}
              onChange={event => handleChangeInput(event, 'question', question.id.value)}
              variant="outlined"
              fullWidth
              margin="dense"
            />
            <FormGroup row>
              <FormControl
                variant="outlined"
                className={classes.mySelect}
              >
                <InputLabel id={question.id.value + 'type'}>Type</InputLabel>
                <Select
                  labelId="type-label"
                  name="type"
                  value={question.type.value}
                  disabled
                  label="Points"
                >
                  {types.map(el => <MenuItem key={el.value} value={el.value}>{el.name}</MenuItem>)}
                </Select>
              </FormControl>
              <FormControl
                error={!question.points.valid && question.points.touched}
                variant="outlined" className={classes.mySelect}
              >
                <InputLabel id="points">Points</InputLabel>
                <Select
                  labelId="points-label"
                  name="points"
                  value={question.points.value}
                  onClick={(event) => handleChangeInput({target: {name: 'points', value: event.target.value || ''}}, 'question', question.id.value)}
                  label="Points"
                >
                  {points.map(el => <MenuItem key={el.value} value={el.value}>{el.name}</MenuItem>)}

                </Select>
                <FormHelperText>{question.points.errorMsg}</FormHelperText>
              </FormControl>

            </FormGroup>
            {question.answers.errorMsg ? 
            (<FormHelperText>{question.answers.errorMsg}</FormHelperText>)
            : null}
            <h5>Answers</h5>
            <FormGroup>
              {question.answers.value.map(answer => (
                <Answer
                  key={answer.id.value} answer={answer} deleteDisabled={question.answers.value.length === 2} questionId={question.id.value}
                  handleChangeInput={handleChangeInput} handleRemoveFields={handleRemoveFields}
                />
          ))}
            </FormGroup>
            <IconButton
              onClick={() => handleAddFields('answer', question.id.value)}
            >
              <AddIcon />
            </IconButton>
          </Grid>
        </CardContent>
      </Card>
    </FormControl>
  )
}
export default Question;

Question.propTypes = {
  handleChangeInput: PropTypes.func.isRequired,
  handleAddFields: PropTypes.func.isRequired,
  handleRemoveFields: PropTypes.func.isRequired,
  number: PropTypes.number.isRequired,
  question: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
}
