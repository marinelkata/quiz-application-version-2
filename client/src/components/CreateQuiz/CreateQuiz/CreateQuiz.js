import React, { useState, useMemo, useCallback } from 'react';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import { Typography, CssBaseline, Grid } from '@material-ui/core';
import PropTypes from 'prop-types';
import cloneDeep from 'lodash/cloneDeep';
import { makeStyles } from '@material-ui/core/styles';
import QuizDetails from '../QuizDetails/QuizDetails';
import Question from '../Question/Question';
import style from './style';
import { validateInput } from '../../../helpers/helpers';
import AlertDialog from '../../AlertDialog/AlertDialog';
const useStyles = makeStyles((theme) => style(theme));

function CreateQuiz({ create, categories }) {

    const classes = useStyles();
    const [alert, setAlert] = useState({open:false, msg: null});
    const [input, setInput] = useState({
        name: {
            value: '',
            type: 'text',
            valid: false,
            touched: false,
            errorMsg: '',
        },
        category: {
            value: '',
            type: 'select',
            valid: false,
            touched: false,
            errorMsg: '',
        },
        duration: {
            value: '',
            type: 'number',
            valid: false,
            touched: false,
            errorMsg: '',
        },
        questions: {
            value: [],
            type: 'questions',
            valid: false,
            touched: false,
            errorMsg: '',
        },
    });
 
    const createNewAnswer = useMemo(() => {
        let id= 0;
        return () => {
            return {
                id: {
                    value: id++,
                },
                text: {
                    value: '',
                    type: 'text',
                    valid: false,
                    touched: false,
                    errorMsg: '',
                },
                value: {
                    value: false,
                    type: 'boolean',
                    valid: true,
                    touched: false,
                }
            }
          };
    },[]);

    const createNewQuestion = useMemo(() => {
        let id = 0;
        return (type) => {
            return {
            id: {
                value: id++,
            },
            text: {
                value: '',
                type: 'text',
                valid: false,
                touched: false,
                errorMsg: '',
            },
            type: {
                value: type,
                type: 'text',
                valid: true,
            },
            points: {
                value: '',
                type: 'select',
                valid: false,
                touched: false,
                errorMsg: ''
            },
            answers: {
                value: [createNewAnswer(), createNewAnswer()],
                type: 'answers',
                valid: false,
                touched: false,
                errorMsg: '',
            }
        }
      };
},[]);

    const handleSubmit = (e) => {
        e.preventDefault();
        const quiz = Object.keys(input).reduce((acc, field) => {
            if (field !== "questions") {
                acc.data[field] = input[field].value;
                acc.errors.push(validateInput(input[field]));
                return acc;
            };
                acc.data.questions = input.questions?.value.reduce((questionsArr, el, index) => {
                    const newQuestion =  Object.keys(el).filter(ml => ml !== "id").reduce((q, ml) => {
                        if (ml !== 'answers') {
                            q[ml] = el[ml].value;
                            acc.errors.push(validateInput(el[ml]));
                            return q;
                        };
                        q.answers = el.answers.value.reduce((answersArr, kv) => {
                            const newAnswer = Object.keys(kv).filter(pm => pm !== "id").reduce((a, pm) => {
                                a[pm] = kv[pm].value;
                                acc.errors.push(validateInput(kv[pm]));
                                return a;
                            }, {});
                        answersArr.push(newAnswer);
                        return answersArr;
                         }, [])
                        return q;
                    }, {});
                    acc.errors.push(validateInput(input.questions.value[index].answers));
                    questionsArr.push(newQuestion);
                    return questionsArr;
                    
                }, []);
            acc.errors.push(validateInput(input.questions));
            return acc;
        }, { errors: [], data: {} });

        const fails = quiz.errors.filter(e => e !== null);

        if (fails.length > 0) {
            return setAlert({open: true, msg: "You must fist complete the form!"});
          } 
        create(quiz.data);
    };

    const changeDetails = useCallback((field, newValue, inputFields) => {
        inputFields[field].value = newValue;
        return validateInput(inputFields[field]);
    },[]);

    const changeQuestion = useCallback((field, newValue, inputFields, questionId) => {
        inputFields.questions.value = inputFields.questions.value.map(question => {
            if (question.id.value === questionId) {
                question[field].value = newValue;
                validateInput(question[field]);
            };
            return question;
        });
    },[]);

    const changeAnswerText = useCallback((field, newValue, inputFields, questionId, answerId) => {
        return inputFields.questions.value = inputFields.questions.value.map(question => {
            if (question.id.value === questionId) {
                question.answers.value.map(answer => {
                    if (answer.id.value === answerId) {
                        answer[field].value = newValue;
                        validateInput(answer[field]);
                    }
                    return answer;
                })
                validateInput(question.answers);
            }
            return question;
        });
    },[]);

    const changeAnswerValue = useCallback((inputFields, questionId, answerId) => {
        return inputFields.questions.value = inputFields.questions.value.map(question => {
            if (question.id.value === questionId) {
                question.type.value === 'multiple'
                    ? question.answers.value.map(answer => {
                        answer.id.value === answerId ? answer.value.value = !answer.value.value : '';
                        return answer;
                    })
                    : question.answers.value.map(answer => {
                        answer.id.value === answerId ? answer.value.value = !answer.value.value : answer.value.value = false;
                        return answer;
                    })
                validateInput(question.answers);
            }
            return question;
        })
    },[]);

    const handleChangeInput = (event, object, id, answerId) => {

        const newInputFields = cloneDeep(input);
        switch (object) {
            case 'input':
                changeDetails(event.target.name, event.target.value, newInputFields);
                break;
            case 'question':
                changeQuestion(event.target.name, event.target.value, newInputFields, id);
                break;
            case 'answer':
                event.target.name
                    ? changeAnswerText(event.target.name, event.target.value, newInputFields, id, answerId)
                    : changeAnswerValue(newInputFields, id, answerId);
                break;
        }
        return setInput(newInputFields);
    };

    const addQuestion = useCallback((inputFields, question) => {
            inputFields.questions.value.push(question);
            validateInput(inputFields.questions);
    },[]);
     

    const addAnswer = useCallback((inputFields, questionId, answer) => {
        return inputFields.questions.value.map(question => {
            if (question.id.value === questionId) {
                question.answers.value.push(answer);
                validateInput(question.answers);
            }
            return question;
        });
    },[]);

    const handleAddFields = (object, value) => {
        const newInputFields = cloneDeep(input);
        switch (object) {
            case 'question':
                addQuestion(newInputFields, createNewQuestion(value));
                break;
            case 'answer':
                addAnswer(newInputFields, value, createNewAnswer());
                break;
        }
        return setInput(newInputFields);
    }

    const removeQuestion = useCallback((inputFields, questionId) => {
        inputFields.questions.value.splice((inputFields.questions.value.findIndex(q => q.id.value === questionId)), 1);
        validateInput(inputFields.questions);
    },[]);

    const removeAnswer = useCallback((inputFields, questionId, answerId) => {
        return inputFields.questions.value.map(question => {
            if (question.id.value === questionId) {
                question.answers.value.splice(question.answers.value.findIndex(a => a.id.value === answerId), 1);
                validateInput(question.answers);
            }
            return question;
        })
    },[]);
    const handleRemoveFields = (object, id, answerId) => {
        const newInputFields = cloneDeep(input);
        switch (object) {
            case 'question':
                removeQuestion(newInputFields, id);
                break;
            case 'answer':
                removeAnswer(newInputFields, id, answerId);
                break;
        }
        return setInput(newInputFields);
    }

    return (
      <Container component="main">
        <AlertDialog alert={alert} close={() => setAlert({open:false, msg: null})} />
        <CssBaseline />
        <div className={classes.myPaper}>
          <Typography component="h1" variant="h2">
            Create Quiz
          </Typography>
          <form className={classes.myForm} onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <QuizDetails classes={classes} input={input} categories={categories} handleChangeInput={handleChangeInput} />
              {input.questions ? input.questions.value.map((question, index) => (
                <Question
                  key={question.id} classes={classes} number={index + 1} question={question}
                  handleChangeInput={handleChangeInput} handleRemoveFields={handleRemoveFields} handleAddFields={handleAddFields}
                />)) : null}
              <Button className='mr-3' variant="outlined" color="primary" onClick={() => handleAddFields('question', 'single')}>
                ADD Single-choice Question
              </Button>
              <Button variant="outlined" color="primary" onClick={() => handleAddFields('question', 'multiple')}>
                ADD Multiple-choice Question
              </Button>
            </Grid>
            <Button
              className={classes.mySubmitButton}
              variant="contained"
              color="primary"
              type="submit"
              onClick={handleSubmit}
            >CREATE QUIZ
            </Button>
          </form>
        </div>
      </Container>
    );
}

export default CreateQuiz;

CreateQuiz.propTypes = {
    create: PropTypes.func.isRequired,
    categories: PropTypes.array.isRequired,
}
