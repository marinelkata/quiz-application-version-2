const style = (theme) => ({
    myPaper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },
      myQuestion: {
        marginTop: theme.spacing(3),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },
      myButton: {
        marginTop: theme.spacing(1),
        marginRight: theme.spacing(10),
      },
      mySubmitButton: {
        alignItems: 'center',
        marginTop: theme.spacing(5),
        marginBottom: theme.spacing(5),
      },
      myForm: {
        width: '100%',
        marginTop: theme.spacing(3),
      },
      mySelect: {
        width: '20%',
        marginBlock: theme.spacing(3),
        marginRight: theme.spacing(3),
      },
      mySubmit: {
        margin: theme.spacing(3, 0, 2),
      },
      mySelectEmpty: {
        marginTop: theme.spacing(2),
      },
      myFormControl: {
        minWidth: '100%',
        marginBottom: theme.spacing(2),
      },
      myCard: {
        width: '100%',
        marginBlock: 20
      },
});

export default style;
