import React, {useContext} from 'react';
import { useHistory } from 'react-router-dom';
import {IconButton, CardContent, CardMedia, Typography, Card, CardHeader } from '@material-ui/core';
import {roles, SERVER_URL} from '../../common/constants';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AuthContext from '../../context/AuthContext';

const useStyles = makeStyles(({
  root: {
    maxWidth: 250,
    marginRight:'20px',
    marginTop:'20px',
  },
  media: {
    height: 160,
  },
  fixedHeight: {
    height: 240,
  },
}));


const Categories = ({categories}) => {
  const classes = useStyles();
  
  const history = useHistory();
  const { user } = useContext(AuthContext);

  return (
    <>
      {categories.map(category => {
        return !(!category.quizzes && user.role === roles.DEFAULT_USER_ROLE) ?
       (
         <Card className={classes.root} key={category.id}>
           <CardHeader
             title={category.name}
             subheader={`Quizzes count: ${category.quizzes}`}
             action={
               <IconButton aria-label="settings" disabled={category.quizzes < 1} onClick={() => history.push(`/categoties/${+category.id}/quizzes?page=1`)}>
                 <MoreVertIcon />
               </IconButton>
            }
           />
           <CardMedia
             className={classes.media}
             image={`${SERVER_URL}${category.cover_path}`}
           />
           <CardContent lassName={classes.fixedHeight}>
             <Typography variant="body2" color="textSecondary" component="p">
               {category.description}
             </Typography>
           </CardContent>
         </Card>
      )  : ''
    })}
    </>
  )
};

Categories.propTypes = {
  categories: PropTypes.array.isRequired,
}
export default Categories;
