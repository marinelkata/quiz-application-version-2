const getToken = () => {
    return localStorage.getItem('token') || '';
};

const setToken = (value) => {
    return localStorage.setItem('token', value);
};

const removeToken = () => {
    return localStorage.removeItem('token');
};

export default {
    getToken,
    setToken,
    removeToken,
};
