import React, {useContext} from 'react';
import { Route, Redirect } from 'react-router-dom';
import AuthContext from './AuthContext';
import PropTypes from 'prop-types';
import NotFound from '../components/NotFound/NotFound';

const GuardedRoute = ({ component: Component, auth, ...rest }) => {
  
  const { isLoggedIn} = useContext(AuthContext);

  return isLoggedIn && !auth 
  ? <NotFound />
  : (
    <Route
      {...rest} render={(props) => {
                return (
                    auth === true
                        ? <Component {...props} />
                        : <Redirect to='/' />
                )
      }}
    />)
};

GuardedRoute.propTypes = {
    component: PropTypes.func.isRequired,
    auth: PropTypes.bool.isRequired,
};

export default GuardedRoute;
