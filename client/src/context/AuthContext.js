import { createContext } from 'react';
import jwtDecode from 'jwt-decode';

export const extractUser = token => {
  try {
    return jwtDecode(token);
  } catch {
    return null;
  }
}

export const AuthContext = createContext({
  isLoggedIn: false,
  user: null,
  setLogin: () => {},
});

export default AuthContext;
